# Personal Project Participation Report

* Subject: Clemens Klug
* Last week: KW 32 (continous from start)

## Activites

### Task 1

* Participated in Team Sonja
* Designed LiLO Architecture [Slide #22](https://vc.uni-bamberg.de/moodle/pluginfile.php/617791/mod_folder/content/0/Team%20Sonja.zip?forcedownload=1)
* Technical support and guidance in group

### Task 2

* Participated in Team Icinga
* Hands-on Icinga in Docker
	* WebUI
	* Configuration Files
	* REST API

### Task 3

* Participated in Android Group
* Leader of Network & Notifications Group
	* support for Web-Application Group for REST offered, but never requested
* Co-lead 'Introduction & Refreshment of Android development' session with Dennis
* Designed Android architecture
* Implemented REST client & unittests (links to current code): [Wiki](https://gitlab.com/living-lab-bamberg/lilo/wikis/Android#rest)
	* [RestClient](https://gitlab.com/living-lab-bamberg/lilo-mobile/tree/master/app/src/main/java/de/uni_bamberg/wiai/mobi/lilo/app/networking)
	* [RestObjects](https://gitlab.com/living-lab-bamberg/lilo-mobile/tree/master/app/src/main/java/de/uni_bamberg/wiai/mobi/lilo/app/data/rest)
* Internal support (esp. regarding notifications)
* Research about alternatives to polling (Polling mails from Gmail-app; Firebase cloud messaging)

### Task 4

* Cleaned up and commented networking part of App (With little documentation, though)

### Task 5

* Work on User Stories #14, #15, #16 with focus on the first two
* Refactoring
	* Multiple Issue pulling: Enable notifications about more than one sensor
	* [NotificationWatchingStrategy](https://gitlab.com/living-lab-bamberg/lilo/wikis/Android#notifications-fetch-and-process): Enable easy replacement of polling-with-intervals by implementing a strategy pattern

## Lessons learned / take away messages

* Work meetings within subteams are sufficient to deliver good standalone solution, but increase hesitations to communicate between subteams
* Organization is vital
* Students don't always recognize that by don't putting in the effort they proposed to not only leads to less progress, but demotivates others to follow